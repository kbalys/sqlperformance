1. Download Microsoft SQL Server Management Studio: https://docs.microsoft.com/en-us/sql/ssms/download-sql-server-management-studio-ssms?view=sql-server-ver16
2. Navigate to folder sqlperformance/scripts
3. Build the docker image
docker build . -t "sqlperformance:latest"
4. Run the container. If you don't use local sql server you may use 1433 port
docker run -p 1434:1433 -d sqlperformance:latest
5. Open Microsoft SQL Server Mangement Studio and log into the container:
server: .,1434
user: sa
Password: SqlP@ssword1234
6. Get container Id
docker container ls
7. Kill the container
docker kill <continer id>
8. Get image id 
docker image ls
9. Remove the image
docker image rm <image id> -f
