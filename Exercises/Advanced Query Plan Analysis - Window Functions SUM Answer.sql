USE AdventureWorks
SET STATISTICS IO, TIME ON
GO

-- With 2012, calculating Running totals by using Aggregare function SUM() with extended Window Function OVER():
-- Version 1
SELECT s.SalesPersonID
	,p.FirstName
	,p.LastName
	,s.OrderDate
	,s.TotalDue
	,SUM(s.TotalDue) OVER (
		PARTITION BY s.SalesPersonID ORDER BY s.OrderDate
			,s.TotalDue
		) AS RunTotal
FROM Sales.SalesOrderHeader s
JOIN Person.Person p ON s.SalesPersonID = p.BusinessEntityID

-- Improvement - see IO Statistics - Worktable
SELECT s.SalesPersonID
	,p.FirstName
	,p.LastName
	,s.OrderDate
	,s.TotalDue
	,SUM(s.TotalDue) OVER (
		PARTITION BY s.SalesPersonID ORDER BY s.OrderDate
			,s.TotalDue ROWS UNBOUNDED PRECEDING
		) AS RunTotal
FROM Sales.SalesOrderHeader s
JOIN Person.Person p ON s.SalesPersonID = p.BusinessEntityID
	
-- Version 2
;WITH RunTotal
AS (
	SELECT ROW_NUMBER() OVER (
			ORDER BY s.OrderDate
				,TotalDue
			) AS [Row]
		,s.SalesPersonID
		,p.FirstName
		,p.LastName
		,s.TotalDue
		,s.OrderDate
	FROM Sales.SalesOrderHeader s
	JOIN Person.Person p ON s.SalesPersonID = p.BusinessEntityID
	)
SELECT SalesPersonID
	,FirstName
	,LastName
	,OrderDate
	,TotalDue
	,SUM(TotalDue) OVER (
		PARTITION BY SalesPersonID ORDER BY [Row] ROWS UNBOUNDED PRECEDING
		) AS SumTotalDue
FROM RunTotal