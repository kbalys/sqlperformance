USE AdventureworksDW
GO

DROP TABLE IF EXISTS dbo.FactInternetSalesBig
GO

CREATE TABLE dbo.FactInternetSalesBig (
	ProductKey int NOT NULL,
	OrderDateKey int NOT NULL,
	DueDateKey int NOT NULL,
	ShipDateKey int NOT NULL,
	CustomerKey int NOT NULL,
	PromotionKey int NOT NULL,
	CurrencyKey int NOT NULL,
	SalesTerritoryKey int NOT NULL,
	SalesOrderNumber nvarchar(40) NOT NULL,
	SalesOrderLineNumber tinyint NOT NULL,
	RevisionNumber tinyint NOT NULL,
	OrderQuantity smallint NOT NULL,
	UnitPrice money NOT NULL,
	ExtendedAmount money NOT NULL,
	UnitPriceDiscountPct float NOT NULL,
	DiscountAmount float NOT NULL,
	ProductStandardCost money NOT NULL,
	TotalProductCost money NOT NULL,
	SalesAmount money NOT NULL,
	TaxAmt money NOT NULL,
	Freight money NOT NULL,
	CarrierTrackingNumber nvarchar(25) NULL,
	CustomerPONumber nvarchar(25) NULL,
	OrderDate datetime NULL,
	DueDate datetime NULL,
	ShipDate datetime NULL
)
GO

INSERT INTO dbo.FactInternetSalesBig
SELECT * FROM dbo.FactInternetSales
GO

DECLARE @i INT = 0
WHILE @i < 7
BEGIN 
	INSERT INTO dbo.FactInternetSalesBig WITH (TABLOCK)
	 SELECT ProductKey
	 ,OrderDateKey
	 ,DueDateKey
	 ,ShipDateKey
	 ,CustomerKey
	 ,PromotionKey
	 ,CurrencyKey
	 ,SalesTerritoryKey
	 ,SalesOrderNumber +CONVERT(NVARCHAR(MAX), @i)
	 ,SalesOrderLineNumber
	 ,RevisionNumber + @i
	 ,OrderQuantity
	 ,UnitPrice
	 ,ExtendedAmount
	 ,UnitPriceDiscountPct
	 ,DiscountAmount
	 ,ProductStandardCost
	 ,TotalProductCost
	 ,SalesAmount
	 ,TaxAmt
	 ,Freight
	 ,CarrierTrackingNumber
	 ,CustomerPONumber 
	 ,OrderDate
	 ,DueDate
	 ,ShipDate
	 FROM dbo.FactInternetSalesBig
 	SET @i = @i+1
	print(@i)
END 

-- can take some time...


	INSERT INTO dbo.FactInternetSalesBig WITH (TABLOCK)
	 SELECT ProductKey
	 ,OrderDateKey
	 ,DueDateKey
	 ,ShipDateKey
	 ,CustomerKey
	 ,PromotionKey
	 ,CurrencyKey
	 ,SalesTerritoryKey
	 ,SalesOrderNumber +CONVERT(NVARCHAR(MAX), 10)
	 ,SalesOrderLineNumber
	 ,RevisionNumber + 10
	 ,OrderQuantity
	 ,UnitPrice
	 ,ExtendedAmount
	 ,UnitPriceDiscountPct
	 ,DiscountAmount
	 ,ProductStandardCost
	 ,TotalProductCost
	 ,SalesAmount
	 ,TaxAmt
	 ,Freight
	 ,CarrierTrackingNumber
	 ,CustomerPONumber 
	 ,OrderDate
	 ,DueDate
	 ,ShipDate
	 FROM dbo.FactInternetSalesBig

