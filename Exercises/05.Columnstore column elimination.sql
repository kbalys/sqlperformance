USE AdventureWorksDW
GO


SELECT * FROM sys.dm_db_column_store_row_group_physical_stats
WHERE object_id = OBJECT_ID('FactInternetSalesTEST')

SET STATISTICS IO ON
SET STATISTICS TIME ON
GO


-- logical reads
SELECT sum(salesamount), count(*), AVG(UnitPrice), AVG(ExtendedAmount), AVG(DiscountAmount), AVG(Freight) 
FROM FactInternetSalesRow

SELECT sum(salesamount)
FROM FactInternetSalesRow




SELECT sum(salesamount), count(*), AVG(UnitPrice), AVG(ExtendedAmount), AVG(DiscountAmount), AVG(Freight) 
FROM FactInternetSalesTEST

SELECT sum(salesamount)
FROM FactInternetSalesTEST