USE AdventureWorks
SET STATISTICS TIME, IO ON
GO

-- lookup
SELECT p.BusinessEntityID
	,p.LastName
	,p.FirstName
	,p.NameStyle
FROM Person.Person AS p
WHERE p.LastName LIKE 'Jaf%';

-- RID lookup
SELECT dl.[DatabaseLogID]
	,dl.[PostTime]
	,dl.[DatabaseUser]
	,dl.[Event]
	,dl.[Schema]
	,dl.[Object]
FROM dbo.DatabaseLog dl
WHERE dl.DatabaseLogID = 121