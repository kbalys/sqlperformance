USE AdventureWorks
SET STATISTICS TIME, IO ON
GO

-- Fix for ID Lookup on Table
CREATE NONCLUSTERED INDEX [IX_Person_LastName] ON [Person].[Person] ([LastName] ASC) INCLUDE (
	[NameStyle]
	,[FirstName]
	)

-- Fix for RID Lookup on Heap
CREATE NONCLUSTERED INDEX [IX_DatabaseLog_DatabaseLogID] ON [dbo].[DatabaseLog] ([DatabaseLogID] ASC) INCLUDE (
	[PostTime]
	,[DatabaseUser]
	,[Event]
	,[Schema]
	,[Object]
	)

-- Clean up
DROP INDEX Person.Person.IX_Person_LastName
DROP INDEX dbo.DatabaseLog.IX_DatabaseLog_DatabaseLogID