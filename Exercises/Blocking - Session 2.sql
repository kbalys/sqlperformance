USE AdventureWorks
SET STATISTICS TIME, IO ON
GO

BEGIN TRAN

DECLARE @next INT

SELECT @next = ID
FROM dbo.NextItem AS ni
WHERE ni.IsUsed = 0
ORDER BY ni.Id DESC

SELECT @next

UPDATE ni
SET ni.IsUsed = 1
FROM dbo.NextItem AS ni
WHERE ni.Id = @next
--COMMIT TRAN
--ROLLBACK TRAN