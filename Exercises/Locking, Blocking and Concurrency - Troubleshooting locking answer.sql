USE SqlServerInternals
SET STATISTICS TIME, IO ON
GO

-- Create index.
CREATE NONCLUSTERED INDEX [IX_DeliveryOrders_OrderNum] ON [Delivery].[Orders] ([OrderNum] ASC) INCLUDE (
	[Amount]
	)

-- Clean Up
DROP INDEX [Delivery].[Orders].[IX_DeliveryOrders_OrderNum]