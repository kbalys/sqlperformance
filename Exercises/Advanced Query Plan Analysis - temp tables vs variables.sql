USE AdventureWorks
SET STATISTICS IO, TIME ON
GO

DECLARE @variable TABLE (
	BusinessEntityID INT NOT NULL
	,LastName NVARCHAR(255) NOT NULL
	)

INSERT @variable
SELECT BusinessEntityID
	,LastName
FROM Person.Person

-- Inefficient nested loop - Estimated number or row = 1 whereas there are ~20 000
SELECT *
FROM @variable t
JOIN Person.Person p ON t.BusinessEntityID = p.BusinessEntityID

-- Better statistics but the same plan
SELECT *
FROM @variable t
JOIN Person.Person p ON t.BusinessEntityID = p.BusinessEntityID
OPTION (RECOMPILE)