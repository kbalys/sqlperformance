--DROP DATABASE Training
--GO

CREATE DATABASE Training
GO

USE Training
GO

-- dbcc page ( {'dbname' | dbid}, filenum, pagenum [, printopt={0|1|2|3} ])

DBCC TRACEON(3604)

DBCC PAGE('Training', 1, 0, 3) -- File Header
DBCC PAGE('Training', 1, 1, 3) -- PFS page approximately 8,000 pages
DBCC PAGE('Training', 1, 2, 3) -- GAM Each GAM covers 64,000 extents, or almost 4 GB of data
DBCC PAGE('Training', 1, 3, 3) -- SGAM Each SGAM covers 64,000 extents, or almost 4 GB of data

DBCC PAGE('Training', 1, 6, 3) -- DIFF
DBCC PAGE('Training', 1, 7, 3)  -- ML


-- mixed extent
SELECT name, is_mixed_page_allocation_on FROM sys.databases












--------------------------------------------------------------------------------------------
DROP TABLE IF EXISTS dbo.Test

CREATE TABLE dbo.Test
(
    Id INT NOT NULL IDENTITY(1, 1),
    Col1 INT NOT NULL,
    Val VARCHAR(200) NOT NULL 
)
GO

DBCC IND('Training', 'dbo.Test', -1)

INSERT dbo.Test(Val, Col1) VALUES (REPLICATE('x', 10), 1)

DBCC IND('Training', 'dbo.Test', -1)

DBCC PAGE('Training', 1, 264, 3)  WITH TABLERESULTS

INSERT dbo.Test(Val, Col1)
SELECT TOP 100 REPLICATE('x', 200), RAND(CHECKSUM(NEWID())) * 100
FROM sys.messages

DBCC IND('Training', 'dbo.Test', -1)







--------------------------------------------------------------------------------------------
--- CLUSTERED INDEX ---

ALTER TABLE [dbo].[Test] ADD CONSTRAINT [PK_Test] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)

SELECT index_id
	,index_level
	,page_count
	,avg_page_space_used_in_percent
	,avg_fragmentation_in_percent
FROM sys.dm_db_index_physical_stats(DB_ID(), OBJECT_ID(N'dbo.Test'), - 1, NULL, 'DETAILED');
go
DBCC IND('Training', 'dbo.Test', -1)

-- root page
DBCC PAGE('Training', 1, 384, 3)  WITH TABLERESULTS

-- leaf level page
DBCC PAGE('Training', 1, 288, 3)  







---------------------------------------------------------------------------------------------
-- NONCLUSTERED INDEX ---  CLUSTERED

CREATE NONCLUSTERED INDEX Idx1 ON dbo.Test(Col1)

DBCC IND('Training', 'dbo.Test', -1)

INSERT dbo.Test(Val, Col1)
SELECT TOP 1000 REPLICATE('x', 200), RAND(CHECKSUM(NEWID())) * 100
FROM sys.messages

-- index 2
DBCC IND('Training', 'dbo.Test', -1)
-- root page
DBCC PAGE('Training', 1, 265, 3) 
-- leaf level page
DBCC PAGE('Training', 1, 266, 3)  





---------------------------------------------------------------------------------------------
-- NONCLUSTERED INDEX ---  HEAP

ALTER TABLE [dbo].[Test] DROP CONSTRAINT [PK_Test] 
GO

DBCC IND('Training', 'dbo.Test', -1)

-- root page
DBCC PAGE('Training', 1, 464, 3) 

-- leaf level page
DBCC PAGE('Training', 1, 432, 3)  


-- NONCLUSTERED INDEX ---  Index Key, covered

set statistics io on

SELECT *
FROM dbo.Test 
WHERE Col1 = 45



SELECT Col1
FROM dbo.Test 
WHERE Col1 = 45


CREATE NONCLUSTERED INDEX Idx2 ON dbo.Test(Col1, Val)

DBCC IND('Training', 'dbo.Test', -1)

-- root/Intermediate page
DBCC PAGE('Training', 1, 480, 3) 
DBCC PAGE('Training', 1, 445, 3) 

CREATE NONCLUSTERED INDEX Idx3 ON dbo.Test(Col1) INCLUDE (Val)

DBCC IND('Training', 'dbo.Test', -1)

-- root/Intermediate page
DBCC PAGE('Training', 1, 608, 3) 
DBCC PAGE('Training', 1, 552, 3) 

-- Index column order

use Training
go

DROP TABLE IF EXISTS test1

create table test1(
	ID INT not null identity(1,1),
	COL1 INT not null,
	COL2 INT not null,
)

INSERT test1(Col1, Col2)
SELECT TOP 10000 RAND(CHECKSUM(NEWID())) * 100, RAND(CHECKSUM(NEWID())) * 100
FROM sys.messages

create nonclustered index idx1 on test1(Col1, Col2) 

select Col1 
from test1 
where Col1 = 1 and col2 = 2

select Col1 
from test1 
where Col1 = 1 or col2 = 2

select Col1 
from test1 
where Col1 = 1 

select Col1 
from test1 
where Col2 = 1 


-- UNIQUE ------------

DROP TABLE IF EXISTS test2
create table test2(
	ID INT not null identity(1,1) PRIMARY KEY,
	COL1 INT not null,
	COL2 INT not null,
)

INSERT test2(Col1, Col2)
SELECT TOP 10000 message_id, RAND(CHECKSUM(NEWID())) * 100
FROM sys.messages

create nonclustered index idx2 on test2(Col1) 
create unique nonclustered index idx3 on test2(Col1) 


DBCC IND('Training', 'dbo.Test2', -1)

-- root/Intermediate page
DBCC PAGE('Training', 1, 672, 3) --idx2 root
DBCC PAGE('Training', 1, 712, 3) --idx3 root


-----------
-- exercise

USE WideWorldImporters
GO

SET STATISTICS IO ON
GO

DBCC freeproccache

SELECT o.OrderId
     , o.ExpectedDeliveryDate
	, o.ContactPersonID
	, o.Comments
	, i.Comments
FROM Sales.Orders AS o 
JOIN Sales.Invoices AS i ON o.OrderID = i.OrderID
WHERE o.OrderDate > '2016-05-01' AND o.ExpectedDeliveryDate > '2016-05-02'


CREATE INDEX index_name ON tableName(column1, column2 ...) INCLUDE (column3, column4...)






















DROP INDEX IF EXISTS Idx1 ON Sales.Orders
DROP INDEX IF EXISTS Idx1 ON Sales.Invoices

CREATE NONCLUSTERED INDEX Idx1 ON Sales.Orders(OrderDate, ExpectedDeliveryDate) INCLUDE (ContactPersonID, Comments)
CREATE NONCLUSTERED INDEX Idx1 ON Sales.Invoices(OrderId) INCLUDE (Comments)


-----------------------------------------------
USE WideWorldImporters 
GO


DBCC FREEPROCCACHE

SELECT  *
FROM Sales.Orders 
WHERE OrderId = 1
-- INCLUDE EXECUTION PLAN

SELECT t.text, * 
FROM sys.dm_exec_cached_plans AS p
CROSS APPLY sys.dm_exec_query_plan(p.plan_handle) AS c
CROSS APPLY sys.dm_exec_sql_text(p.plan_handle) AS t

USE WideWorldImporters 
GO

SELECT *
FROM   sys.dm_os_buffer_descriptors 
WHERE database_id = DB_ID()

DBCC DROPCLEANBUFFERS

--- 

 
select *
from sys.dm_exec_query_optimizer_info
where counter in (
'optimizations'
, 'trivial plan'
, 'search 0'
, 'search 1'
, 'search 2'
)
order by [counter]

SET STATISTICS IO,TIME  OFF
DBCC TRACEON (3604) WITH NO_INFOMSGS;
DBCC TRACEON (8675) WITH NO_INFOMSGS

SELECT * 
FROM Sales.Orders AS o
JOIN Sales.Invoices AS i ON o.OrderID = i.OrderID
JOIN Application.People As p on o.PickedByPersonID = p.PersonID
WHERE o.OrderID = 54

SELECT * 
FROM Sales.Orders AS o
JOIN Sales.Invoices AS i ON o.OrderID = i.OrderID
JOIN Application.People As p on o.PickedByPersonID = p.PersonID
WHERE o.OrderID = 54 AND 1 = 0

SELECT * 
FROM Sales.Orders AS o

SELECT * 
FROM Sales.Orders AS o
WHERE  o.OrderID = 54

--------------------------------------------

DBCC OPTIMIZER_WHATIF(CPUs, 128) WITH NO_INFOMSGS;
DBCC OPTIMIZER_WHATIF(Status)
DBCC OPTIMIZER_WHATIF(ResetAll) WITH NO_INFOMSGS;


DBCC OPTIMIZER_WHATIF(CPUs, 128) WITH NO_INFOMSGS;

SELECT * 
FROM Sales.Orders AS o
INNER HASH JOIN Sales.Invoices AS i ON o.OrderID = i.OrderID
option(recompile)

DBCC OPTIMIZER_WHATIF(CPUs, 1) WITH NO_INFOMSGS;

SELECT * 
FROM Sales.Orders AS o
INNER HASH JOIN Sales.Invoices AS i ON o.OrderID = i.OrderID
option(recompile)

DBCC OPTIMIZER_WHATIF(ResetAll) WITH NO_INFOMSGS;

