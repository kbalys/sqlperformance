USE AdventureWorks
SET STATISTICS IO, TIME ON
GO

DECLARE @paramValues INT = 1
DECLARE @paramTypes NVARCHAR(MAX)

WHILE (@paramValues < 100)
BEGIN
	SET @paramTypes = N'@p0 nvarchar(' + CONVERT(NVARCHAR(10), @paramValues) + ')'

	EXEC sp_executesql N'SELECT * FROM Person.Person p WHERE p.BusinessEntityID = @p0'
		,@paramTypes
		,@p0 = @paramValues

	SET @paramValues += 1
END

SELECT objtype
	,p.size_in_bytes
	,[sql].[text]
FROM sys.dm_exec_cached_plans p
OUTER APPLY sys.dm_exec_sql_text(p.plan_handle) sql
WHERE [Text] LIKE '%SELECT * FROM Person.Person p%'