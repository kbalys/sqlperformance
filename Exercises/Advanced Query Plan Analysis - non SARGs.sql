USE AdventureWorks
SET STATISTICS TIME, IO ON
DBCC DROPCLEANBUFFERS
DBCC FREEPROCCACHE
GO

-- Index Seek
SELECT *
FROM Person.Person
WHERE FirstName = 'Gail'
	AND LastName = 'Erickson'

-- Index Scan
SELECT *
FROM Person.Person
WHERE FirstName + ' ' + LastName = 'Gail Erickson'

-- Index Seek
SELECT *
FROM Person.Person
WHERE LastName LIKE 'E%'

-- Index Scan
SELECT *
FROM Person.Person
WHERE LEFT(LastName, 1) = 'E'

-------------------------------
-- Fix this query to be faster
SELECT w.ProductID
	,w.StartDate
FROM Production.WorkOrder w
WHERE YEAR(w.StartDate) = 2013