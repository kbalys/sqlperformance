USE AdventureWorks
SET STATISTICS IO, TIME ON
-- Query
-- Get first PurchaseOrderDetails items younger than 2014-06-01
-- whose sum is less or equal than 2000 - in other words exclude all details
-- if sum of orders exceeds 2000
GO

-- ROWS UNBOUNDED PRECEDING
-- Calculate number of items per Purchase Order as Aggregate Sum
-- ROWS UNBOUNDED PRECEDING, uses Memory - check IO Statistics
-- without p.ProductId inside OVER results would be different
DECLARE @result2 TABLE (
	PurchaseOrderid INT
	,PurchaseOrderDetailId INT
	,ProductId INT
	,OrderQty INT
	,AggSum INT
	)

PRINT 'ROWS UNBOUNDED PRECEDING'
;WITH cte
AS (
	SELECT p.PurchaseOrderid
		,p.PurchaseOrderDetailId
		,p.ProductId
		,p.OrderQty
		,SUM(p.OrderQty) OVER (
			PARTITION BY p.PurchaseOrderid ORDER BY p.OrderQty
				,p.ProductId ROWS UNBOUNDED PRECEDING
			) AS AggSum
	FROM Purchasing.PurchaseOrderDetail p
	WHERE p.DueDate >= '2014-06-01'
	)
INSERT @result2
SELECT *
FROM cte
WHERE AggSum <= 2000
ORDER BY cte.PurchaseOrderid
	,cte.OrderQty
GO

-- RANGE UNBOUNDED PRECEDING
-- Calculate number of items per Purchase Order as Aggregate Sum
-- RANGE UNBOUNDED PRECEDING - default, uses TEMP DB
DECLARE @result1 TABLE (
	PurchaseOrderid INT
	,PurchaseOrderDetailId INT
	,ProductId INT
	,OrderQty INT
	,AggSum INT
	)

PRINT 'RANGE UNBOUNDED PRECEDING'
;WITH cte
AS (
	SELECT p.PurchaseOrderid
		,p.PurchaseOrderDetailId
		,p.ProductId
		,p.OrderQty
		,SUM(p.OrderQty) OVER (
			PARTITION BY p.PurchaseOrderId ORDER BY p.OrderQty
				,p.ProductId -- default = RANGE UNBOUNDED PRECEDING
			) AS AggSum
	FROM Purchasing.PurchaseOrderDetail p
	WHERE p.DueDate >= '2014-06-01'
	)
INSERT @result1
SELECT *
FROM cte
WHERE AggSum <= 2000
ORDER BY cte.PurchaseOrderid
	,cte.OrderQty
GO

-- Subselect
-- The same as subselect
DECLARE @result3 TABLE (
	PurchaseOrderid INT
	,PurchaseOrderDetailId INT
	,ProductId INT
	,OrderQty INT
	,AggSum INT
	)

PRINT 'Subselect'
;WITH cte
AS (
	SELECT p.PurchaseOrderid
		,p.PurchaseOrderDetailId
		,p.ProductId
		,p.OrderQty
		,(
			SELECT SUM(p1.OrderQty)
			FROM Purchasing.PurchaseOrderDetail p1
			WHERE p1.purchaseOrderId = p.PurchaseOrderId
				AND (
					p1.OrderQty < p.OrderQty
					OR p1.OrderQty = p.OrderQty
					AND p1.ProductId <= p.ProductId
					)
			) AS AggSum
	FROM Purchasing.PurchaseOrderDetail p
	WHERE p.DueDate >= '2014-06-01'
	)
INSERT @result3
SELECT *
FROM cte
WHERE AggSum <= 2000
ORDER BY cte.PurchaseOrderid
	,cte.OrderQty

GO

-- Cursor
DECLARE @result4 TABLE (
	PurchaseOrderid INT
	,PurchaseOrderDetailId INT
	,ProductId INT
	,OrderQty INT
	,AggSum INT
	)

PRINT 'Cursor'
DECLARE @PreviousPurchaseOrderid INT = 0
DECLARE @PurchaseOrderid INT = 0
DECLARE @PurchaseOrderDetailId INT = 0
DECLARE @ProductId INT = 0
DECLARE @OrderQty INT = 0
DECLARE @sum INT = 0

DECLARE c CURSOR
FOR
SELECT p.PurchaseOrderid
	,p.PurchaseOrderDetailId
	,p.ProductId
	,p.OrderQty
FROM Purchasing.PurchaseOrderDetail p
WHERE p.DueDate >= '2014-06-01'
ORDER BY p.PurchaseOrderid
	,p.OrderQty
	,p.ProductId

OPEN c

FETCH NEXT
FROM c
INTO @PurchaseOrderid
	,@PurchaseOrderDetailId
	,@ProductId
	,@OrderQty

WHILE @@FETCH_STATUS = 0
BEGIN
	IF (@PreviousPurchaseOrderid <> @PurchaseOrderid)
	BEGIN
		SET @PreviousPurchaseOrderid = @PurchaseOrderid
		SET @sum = 0
	END

	SET @Sum = @Sum + @OrderQty

	IF (@Sum < 2000)
	BEGIN
		INSERT @result4
		VALUES (
			@PurchaseOrderid
			,@PurchaseOrderDetailId
			,@ProductId
			,@OrderQty
			,@sum
			)
	END

	FETCH NEXT
	FROM c
	INTO @PurchaseOrderid
		,@PurchaseOrderDetailId
		,@ProductId
		,@OrderQty
END

CLOSE c

DEALLOCATE c