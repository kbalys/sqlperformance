SET NOCOUNT ON;

IF DB_ID(N'testwindow') IS NULL
	CREATE DATABASE testwindow;
GO

USE testwindow;
GO

-- GetNums helper function
DROP FUNCTION IF EXISTS dbo.GetNums;
GO

CREATE FUNCTION dbo.GetNums (
    @low AS BIGINT
    ,@high AS BIGINT
    )
RETURNS TABLE
AS
RETURN
WITH L0 AS (
	   SELECT c
	   FROM (
		  SELECT 1
				
		  UNION ALL
				
		  SELECT 1
		  ) AS D(c)
	   )
    ,L1 AS (
	   SELECT 1 AS c
	   FROM L0 AS A
	   CROSS JOIN L0 AS B
	   )
    ,L2 AS (
	   SELECT 1 AS c
	   FROM L1 AS A
	   CROSS JOIN L1 AS B
	   )
    ,L3 AS (
	   SELECT 1 AS c
	   FROM L2 AS A
	   CROSS JOIN L2 AS B
	   )
    ,L4 AS (
	   SELECT 1 AS c
	   FROM L3 AS A
	   CROSS JOIN L3 AS B
	   )
    ,L5 AS (
	   SELECT 1 AS c
	   FROM L4 AS A
	   CROSS JOIN L4 AS B
	   )
    ,Nums AS (
	   SELECT ROW_NUMBER() OVER (
				ORDER BY (
					   SELECT NULL
					   )
				) AS rownum
	   FROM L5
	   )

SELECT TOP (@high - @low + 1) @low + rownum - 1 AS n
FROM Nums
ORDER BY rownum;
GO

-- Transactions table with 10M rows (200 accounts x 50000 transactions per act)
-- Traditional rowstore B-tree index
DROP TABLE IF EXISTS dbo.Transactions;
GO
CREATE TABLE dbo.Transactions (
    actid INT NOT NULL
    ,tranid INT NOT NULL
    ,val MONEY NOT NULL
    ,CONSTRAINT PK_Transactions PRIMARY KEY (
	   actid
	   ,tranid
	   )
    );
GO

DECLARE @num_partitions AS INT = 200
	,@rows_per_partition AS INT = 50000;

INSERT INTO dbo.Transactions
WITH (TABLOCK) (
		actid
		,tranid
		,val
		)
SELECT NP.n
	,RPP.n
	,(ABS(CHECKSUM(NEWID()) % 2) * 2 - 1) * (1 + ABS(CHECKSUM(NEWID()) % 5))
FROM dbo.GetNums(1, @num_partitions) AS NP
CROSS JOIN dbo.GetNums(1, @rows_per_partition) AS RPP;
GO

-- TransactionsCS

-- Clustered columnstore index

DROP TABLE IF EXISTS dbo.TransactionsCS;
GO
SELECT * INTO dbo.TransactionsCS FROM dbo.Transactions;
CREATE CLUSTERED COLUMNSTORE INDEX idx_cs ON dbo.TransactionsCS;

-- TransactionsDCS

-- Traditional rowstore B-tree index

--   + dummy empty filtered nonclustered columnstore index

--     to enable using batch mode operators

DROP TABLE IF EXISTS dbo.TransactionsDCS;
GO
SELECT * INTO dbo.TransactionsDCS FROM dbo.Transactions;

ALTER TABLE dbo.TransactionsDCS
ADD CONSTRAINT PK_TransactionsDCS PRIMARY KEY(actid, tranid);

CREATE NONCLUSTERED COLUMNSTORE INDEX idx_cs_dummy ON dbo.TransactionsDCS(actid)
WHERE actid = -1 AND actid = -2;


SELECT actid, tranid, val, SUM(val) OVER(PARTITION BY actid) AS acttotal
FROM dbo.Transactions;

SELECT actid, tranid, val, SUM(val) OVER(PARTITION BY actid) AS acttotal
FROM dbo.TransactionsCS;

SELECT actid, tranid, val, SUM(val) OVER(PARTITION BY actid) AS acttotal
FROM dbo.TransactionsDCS;