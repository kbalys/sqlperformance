USE AdventureWorks
SET STATISTICS IO, TIME ON
GO

DROP PROCEDURE IF EXISTS ComplexProcedure 
GO

CREATE PROCEDURE ComplexProcedure @productId INT NULL
	,@salesOrderId INT NULL
AS
BEGIN
	SELECT s.ProductID
		,s.LineTotal
		,s.OrderQty
	FROM sales.SalesOrderDetail AS s
	WHERE (
			s.productId = @productId
			OR @productId IS NULL
			)
		AND (
			s.SalesOrderID = @salesOrderId
			OR @salesOrderId IS NULL
			)
END
GO

DBCC FREEPROCCACHE

EXEC ComplexProcedure 772, NULL
EXEC ComplexProcedure NULL, 43661

DBCC FREEPROCCACHE

EXEC ComplexProcedure NULL, 43661
EXEC ComplexProcedure 772, NULL

DROP PROCEDURE IF EXISTS ComplexProcedure 
GO
