USE AdventureWorks
SET STATISTICS TIME, IO ON
GO

-- SET STATISTICS XML ON 
-- SET STATISTICS PROFILE ON
-- click Display Estimated Execution Plan
-- click Include Actual Execution Plan
-- click Include Live Query Statistics
-- click Include Client Statistics

SELECT *
FROM [Person].[Person] AS p
WHERE p.LastName = 'Abel'

-- Now click on Display Estimated Execution Plan and compare it with previous one