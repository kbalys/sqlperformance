USE AdventureWorks
SET STATISTICS IO, TIME ON
GO

SELECT ProductId
	,dbo.ufnGetStock(ProductId)
FROM Sales.SalesOrderDetail

SELECT ProductId
	,tvf.*
FROM Sales.SalesOrderDetail
CROSS APPLY dbo.tvf_GetStock(ProductId) AS tvf

DROP FUNCTION IF EXISTS dbo.tvf_GetStock 
GO