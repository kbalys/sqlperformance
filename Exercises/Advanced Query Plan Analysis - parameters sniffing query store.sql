USE AdventureWorks
SET STATISTICS IO, TIME ON
GO

DROP PROCEDURE IF EXISTS dbo.AddressByCity 
GO

CREATE PROCEDURE dbo.AddressByCity (@City NVARCHAR(30))
AS
SELECT *
FROM Person.Address
WHERE City = @City
GO

-- Execute procedures - the order matters
DBCC DROPCLEANBUFFERS
DBCC FREEPROCCACHE 

EXEC dbo.AddressByCity 'Mentor' 
EXEC dbo.AddressByCity 'London'

-- Execute procedures - the order matters
DBCC FREEPROCCACHE
DBCC DROPCLEANBUFFERS 

EXEC dbo.AddressByCity 'London'
EXEC dbo.AddressByCity 'Mentor' 

-- when testing Query Store do not drop and recreate procedure!