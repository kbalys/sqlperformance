USE AdventureWorks
SET STATISTICS IO, TIME ON
GO

-- view is expanded
SELECT *
FROM [Production].[vProductAndDescription]

-- Try to block expanding. Compare reads and plans
SELECT *
FROM [Production].[vProductAndDescription] WITH (NOEXPAND)

-- This view is not expanded
SELECT *
FROM [Person].[vStateProvinceCountryRegion]

-- A query similar to content of a indexed view is also changed to query to indexed view
SELECT sp.StateProvinceID
	,sp.StateProvinceCode
	,sp.IsOnlyStateProvinceFlag
	,sp.[Name] AS StateProvinceName
	,sp.TerritoryID
	,cr.CountryRegionCode
	,cr.[Name] AS CountryRegionName
FROM Person.StateProvince AS sp
INNER JOIN Person.CountryRegion AS cr ON sp.CountryRegionCode = cr.CountryRegionCode