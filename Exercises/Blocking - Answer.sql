USE AdventureWorks
SET STATISTICS TIME, IO ON
GO

-- Step 1
-- Change transaction isolation level but this causes deadlock
SET TRANSACTION ISOLATION LEVEL SERIALIZABLE

-- Step 2
-- Replace 
SELECT @next = ID
FROM dbo.NextItem AS ni
-- in Session 1 and Session 2 with
SELECT @next = ID
FROM dbo.NextItem AS ni WITH (UPDLOCK)


-- Better solution
-- Do not write such code - use sequences!
CREATE SEQUENCE [dbo].[Sequence1] START
	WITH 1 INCREMENT BY 1

SELECT NEXT VALUE
FOR dbo.Sequence1