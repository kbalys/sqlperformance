USE AdventureWorks
SET STATISTICS IO, TIME ON
GO

-- for each Sales Order return the most expensive line
SELECT soh.SalesOrderID
	,soh.CustomerID
	,x.SalesOrderDetailID
	,x.LineTotal
FROM Sales.SalesOrderHeader AS soh
CROSS APPLY (
	SELECT TOP 1 sod.SalesOrderID
		,sod.SalesOrderDetailID
		,sod.LineTotal
	FROM Sales.SalesOrderDetail sod
	WHERE sod.SalesOrderID = soh.SalesOrderID
	ORDER BY sod.LineTotal DESC, sod.SalesOrderDetailID
	) AS x
ORDER BY soh.SalesOrderID
	-- Rewrite it using ROW_NUMBER()