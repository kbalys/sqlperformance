USE AdventureWorksDW
GO

SELECT count(*) FROM dbo.FactInternetSalesBig

DROP TABLE IF EXISTS FactInternetSalesTEST
GO

-- 10mln
SELECT TOP 10000000 *
INTO FactInternetSalesTEST
FROM FactInternetSalesBig

SELECT count(*) FROM FactInternetSalesTEST

EXEC sp_spaceused 'FactInternetSalesTEST'
--1,6 GB 
set statistics io on

SELECT d.CalendarYear, d.EnglishMonthName, count(*), sum(f.salesamount)
FROM FactInternetSalesTEST AS f
JOIN DimDate AS d ON d.DateKey = f.OrderDateKey
GROUP BY d.CalendarYear, d.EnglishMonthName
ORDER BY d.CalendarYear, d.EnglishMonthName


ALTER TABLE FactInternetSalesTEST ADD  CONSTRAINT PK_FactInternetSalesTEST_SalesOrderNumber_SalesOrderLineNumber 
PRIMARY KEY CLUSTERED (SalesOrderNumber, SalesOrderLineNumber)
GO


EXEC sp_spaceused 'FactInternetSalesTEST'

SELECT d.CalendarYear, d.EnglishMonthName, count(*), sum(f.salesamount)
FROM FactInternetSalesTEST AS f
JOIN DimDate AS d ON d.DateKey = f.OrderDateKey
GROUP BY d.CalendarYear, d.EnglishMonthName
ORDER BY d.CalendarYear, d.EnglishMonthName


CREATE NONCLUSTERED COLUMNSTORE INDEX NCCI ON FactInternetSalesTEST
(	[ProductKey],	[OrderDateKey],	[DueDateKey],	[ShipDateKey],	[CustomerKey],	[PromotionKey],	[CurrencyKey],
	[SalesTerritoryKey],	[SalesOrderNumber],	[SalesOrderLineNumber],	[RevisionNumber],	[OrderQuantity],
	[UnitPrice],	[ExtendedAmount],	[UnitPriceDiscountPct],	[DiscountAmount],	[ProductStandardCost],
	[TotalProductCost],	[SalesAmount],	[TaxAmt],	[Freight],	[CarrierTrackingNumber],	[CustomerPONumber],
	[OrderDate],	[DueDate],	[ShipDate]
) 
GO

EXEC sp_spaceused 'FactInternetSalesTEST'
 

SELECT d.CalendarYear, d.EnglishMonthName, count(*), sum(f.salesamount)
FROM FactInternetSalesTEST AS f
JOIN DimDate AS d ON d.DateKey = f.OrderDateKey
GROUP BY d.CalendarYear, d.EnglishMonthName
ORDER BY d.CalendarYear, d.EnglishMonthName


-- Failed
CREATE CLUSTERED COLUMNSTORE INDEX CCI
ON FactInternetSalesTEST
GO


DROP INDEX NCCI ON FactInternetSalesTEST
GO

ALTER TABLE [dbo].[FactInternetSalesTEST] 
DROP CONSTRAINT [PK_FactInternetSalesTEST_SalesOrderNumber_SalesOrderLineNumber]
GO

CREATE CLUSTERED COLUMNSTORE INDEX CCI
ON FactInternetSalesTEST
GO

EXEC sp_spaceused 'FactInternetSalesTEST'

SELECT d.CalendarYear, d.EnglishMonthName, count(*), sum(f.salesamount)
FROM FactInternetSalesTEST AS f
JOIN DimDate AS d ON d.DateKey = f.OrderDateKey
GROUP BY d.CalendarYear, d.EnglishMonthName
ORDER BY d.CalendarYear, d.EnglishMonthName