USE AdventureWorks
SET STATISTICS TIME, IO ON
GO

-- Nested Loop 
SELECT e.JobTitle
	,a.City
	,p.LastName + ', ' + p.FirstName AS EmployeeName
FROM HumanResources.Employee AS e
JOIN Person.BusinessEntityAddress AS bea ON e.BusinessEntityID = bea.BusinessEntityID
JOIN Person.Address a ON bea.AddressID = a.AddressID
JOIN Person.Person AS p ON e.BusinessEntityID = p.BusinessEntityID;

-- Hash Match 
SELECT *
FROM Person.BusinessEntityAddress AS bea
JOIN Person.Address a ON bea.AddressID = a.AddressID

-- Merge Join
SELECT c.CustomerID
FROM Sales.SalesOrderDetail od
JOIN Sales.SalesOrderHeader oh ON od.SalesOrderID = oh.SalesOrderID
JOIN Sales.Customer c ON oh.CustomerID = c.CustomerID