USE AdventureWorks
SET STATISTICS TIME, IO ON

-- SET STATISTICS XML ON
-- SET STATISTICS PROFILE ON
-- Removes all clean buffers from the buffer pool
DBCC DROPCLEANBUFFERS

-- Removes all elements from the plan cache
DBCC FREEPROCCACHE
-- Removes a specific plan from cache
-- DBCC FREEPROCCACHE [Plan Handle]
GO

-- Compare how much cost from batch each of the following queries take.
SELECT Businessentityid
	,FirstName
	,LastName
	,StateProvinceName
FROM HumanResources.vEmployee
WHERE StateProvinceName = 'Washington'

SELECT *
FROM HumanResources.vEmployee
WHERE StateProvinceName = 'Washington'