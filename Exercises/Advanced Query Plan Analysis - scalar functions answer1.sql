USE AdventureWorks
SET STATISTICS IO, TIME ON
GO

DROP FUNCTION IF EXISTS dbo.tvf_GetStock 
GO

CREATE FUNCTION dbo.tvf_GetStock (@ProductId INT)
RETURNS TABLE
AS
RETURN (
		SELECT ISNULL(SUM(p.[Quantity]), 0) AS Quantity
		FROM [Production].[ProductInventory] p
		WHERE p.[ProductID] = @ProductID
			AND p.[LocationID] = '6'
		)
GO

DECLARE @ProductId INT = 1

SELECT dbo.ufnGetStock(@ProductId)

SELECT *
FROM dbo.tvf_GetStock(@ProductId)