USE AdventureWorks
SET STATISTICS TIME, IO ON
GO

-- This will be probably a parallelised version
SELECT sod.*
FROM Sales.SalesOrderDetail AS sod
ORDER BY sod.ProductID

-- This will be a single thread version
SELECT sod.*
FROM Sales.SalesOrderDetail AS sod
ORDER BY sod.ProductID
OPTION (MAXDOP 1)