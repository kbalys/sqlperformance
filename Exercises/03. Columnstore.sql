

USE AdventureWorksDW
GO

DROP TABLE IF EXISTS FactInternetSalesTEST
GO

CREATE TABLE dbo.FactInternetSalesTEST(
	[ProductKey] [int] NOT NULL,
	[OrderDateKey] [int] NOT NULL,
	[DueDateKey] [int] NOT NULL,
	[ShipDateKey] [int] NOT NULL,
	[CustomerKey] [int] NOT NULL,
	[PromotionKey] [int] NOT NULL,
	[CurrencyKey] [int] NOT NULL,
	[SalesTerritoryKey] [int] NOT NULL,
	[SalesOrderNumber] [nvarchar](40) NOT NULL,
	[SalesOrderLineNumber] [tinyint] NOT NULL,
	[RevisionNumber] [tinyint] NOT NULL,
	[OrderQuantity] [smallint] NOT NULL,
	[UnitPrice] [money] NOT NULL,
	[ExtendedAmount] [money] NOT NULL,
	[UnitPriceDiscountPct] [float] NOT NULL,
	[DiscountAmount] [float] NOT NULL,
	[ProductStandardCost] [money] NOT NULL,
	[TotalProductCost] [money] NOT NULL,
	[SalesAmount] [money] NOT NULL,
	[TaxAmt] [money] NOT NULL,
	[Freight] [money] NOT NULL,
	[CarrierTrackingNumber] [nvarchar](25) NULL,
	[CustomerPONumber] [nvarchar](25) NULL,
	[OrderDate] [datetime] NULL,
	[DueDate] [datetime] NULL,
	[ShipDate] [datetime] NULL,
	INDEX CCI CLUSTERED COLUMNSTORE -- COLUMNSTORE
) ON [PRIMARY]
GO


SELECT * FROM sys.column_store_row_groups
WHERE object_id = OBJECT_ID('FactInternetSalesTEST')


-- rows < 102 400 
INSERT INTO FactInternetSalesTEST
SELECT TOP 102399 *
FROM FactInternetSalesBig


SELECT * FROM sys.column_store_row_groups 
WHERE object_id = OBJECT_ID('FactInternetSalesTEST')


SELECT d.CalendarYear, d.EnglishMonthName, count(*), sum(f.salesamount)
FROM FactInternetSalesTEST AS f
JOIN DimDate AS d ON d.DateKey = f.OrderDateKey
GROUP BY d.CalendarYear, d.EnglishMonthName
ORDER BY d.CalendarYear, d.EnglishMonthName




-- Cleanup
TRUNCATE TABLE FactInternetSalesTEST

ALTER INDEX CCI ON FactInternetSalesTEST 
REBUILD PARTITION = ALL WITH (DATA_COMPRESSION = COLUMNSTORE)
GO





-- 102400 rows
INSERT INTO FactInternetSalesTEST
SELECT TOP 102400 *
FROM FactInternetSalesBig


SELECT * FROM sys.column_store_row_groups 
WHERE object_id = OBJECT_ID('FactInternetSalesTEST')

-- transition_to_compressed_state_desc
SELECT * FROM sys.dm_db_column_store_row_group_physical_stats
WHERE object_id = OBJECT_ID('FactInternetSalesTEST')

SELECT d.CalendarYear, d.EnglishMonthName, count(*), sum(f.salesamount)
FROM FactInternetSalesTEST AS f
JOIN DimDate AS d ON d.DateKey = f.OrderDateKey
GROUP BY d.CalendarYear, d.EnglishMonthName
ORDER BY d.CalendarYear, d.EnglishMonthName



-- Cleanup
TRUNCATE TABLE FactInternetSalesTEST

ALTER INDEX CCI ON FactInternetSalesTEST 
REBUILD PARTITION = ALL WITH (DATA_COMPRESSION = COLUMNSTORE)
GO



-- multiple inserts
INSERT INTO FactInternetSalesTEST
SELECT TOP 102399 *
FROM FactInternetSalesBig
GO 10



-- open
SELECT * FROM sys.dm_db_column_store_row_group_physical_stats
WHERE object_id = OBJECT_ID('FactInternetSalesTEST')



SELECT d.CalendarYear, d.EnglishMonthName, count(*), sum(f.salesamount)
FROM FactInternetSalesTEST AS f
JOIN DimDate AS d ON d.DateKey = f.OrderDateKey
GROUP BY d.CalendarYear, d.EnglishMonthName
ORDER BY d.CalendarYear, d.EnglishMonthName

-- max - count
SELECT 1048576  - COUNT(*) FROM FactInternetSalesTEST


-- +1
INSERT INTO FactInternetSalesTEST
SELECT TOP 24587 *
FROM FactInternetSalesBig
GO 


-- closed
SELECT * FROM sys.dm_db_column_store_row_group_physical_stats
WHERE object_id = OBJECT_ID('FactInternetSalesTEST')


SELECT d.CalendarYear, d.EnglishMonthName, count(*), sum(f.salesamount)
FROM FactInternetSalesTEST AS f
JOIN DimDate AS d ON d.DateKey = f.OrderDateKey
GROUP BY d.CalendarYear, d.EnglishMonthName
ORDER BY d.CalendarYear, d.EnglishMonthName


-- Cleanup
TRUNCATE TABLE FactInternetSalesTEST

ALTER INDEX CCI ON FactInternetSalesTEST 
REBUILD PARTITION = ALL WITH (DATA_COMPRESSION = COLUMNSTORE)
GO








-- D/ Wstawienie wierszy - poni�ej progu 102 400 i wymuszenie kompresji
INSERT INTO FactInternetSalesTEST
SELECT TOP 102399 *
FROM FactInternetSalesBig
GO 3

SELECT * FROM sys.dm_db_column_store_row_group_physical_stats
WHERE object_id = OBJECT_ID('FactInternetSalesTEST')


ALTER INDEX CCI ON FactInternetSalesTEST
REORGANIZE WITH ( COMPRESS_ALL_ROW_GROUPS = ON )
GO

SELECT * FROM sys.dm_db_column_store_row_group_physical_stats
WHERE object_id = OBJECT_ID('FactInternetSalesTEST')




-- Cleanup
TRUNCATE TABLE FactInternetSalesTEST

ALTER INDEX CCI ON FactInternetSalesTEST 
REBUILD PARTITION = ALL WITH (DATA_COMPRESSION = COLUMNSTORE)
GO





-- max + 1 TABLOCK
INSERT INTO FactInternetSalesTEST WITH (TABLOCK)
SELECT TOP 1048576 *
FROM FactInternetSalesBig


-- TABLOCK
SELECT * FROM sys.dm_db_column_store_row_group_physical_stats
WHERE object_id = OBJECT_ID('FactInternetSalesTEST')


-- compressed_stated_desc = MERGE
ALTER INDEX CCI ON FactInternetSalesTEST
REORGANIZE  WITH ( COMPRESS_ALL_ROW_GROUPS = ON )
GO

SELECT * FROM sys.dm_db_column_store_row_group_physical_stats
WHERE object_id = OBJECT_ID('FactInternetSalesTEST')


-- skasowanie niemal wszystkich wierszy
DELETE TOP (1048575) FROM FactInternetSalesTEST
GO

-- deleted_rows
SELECT * FROM sys.dm_db_column_store_row_group_physical_stats
WHERE object_id = OBJECT_ID('FactInternetSalesTEST')


ALTER INDEX CCI ON FactInternetSalesTEST
REORGANIZE  
GO


SELECT * FROM sys.dm_db_column_store_row_group_physical_stats
WHERE object_id = OBJECT_ID('FactInternetSalesTEST')










