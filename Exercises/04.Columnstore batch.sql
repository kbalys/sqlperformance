USE AdventureWorksDW
GO

-- Cleanup
TRUNCATE TABLE FactInternetSalesTEST

ALTER INDEX CCI ON FactInternetSalesTEST 
REBUILD PARTITION = ALL WITH (DATA_COMPRESSION = COLUMNSTORE)
GO

-- Setup
INSERT INTO FactInternetSalesTEST 
SELECT TOP 1048575 *
FROM FactInternetSalesBig

DROP TABLE IF EXISTS FactInternetSalesRow 

SELECT *
INTO FactInternetSalesRow 
FROM FactInternetSalesTEST


SELECT * FROM sys.dm_db_column_store_row_group_physical_stats
WHERE object_id = OBJECT_ID('FactInternetSalesTEST')

SELECT count(*) FROM FactInternetSalesTEST
SELECT count(*) FROM FactInternetSalesRow

CREATE CLUSTERED INDEX Idx1 ON FactInternetSalesRow (OrderDateKey) 




-- A/ BatchMode Processing
-----------------------------------
SET STATISTICS IO ON 


SELECT d.CalendarYear, d.EnglishMonthName, count(*), sum(f.salesamount)
FROM FactInternetSalesRow AS f
JOIN DimDate AS d ON d.DateKey = f.OrderDateKey
GROUP BY d.CalendarYear, d.EnglishMonthName
ORDER BY d.CalendarYear, d.EnglishMonthName



SELECT d.CalendarYear, d.EnglishMonthName, count(*), sum(f.salesamount)
FROM FactInternetSalesTEST AS f
JOIN DimDate AS d ON d.DateKey = f.OrderDateKey
GROUP BY d.CalendarYear, d.EnglishMonthName
ORDER BY d.CalendarYear, d.EnglishMonthName
