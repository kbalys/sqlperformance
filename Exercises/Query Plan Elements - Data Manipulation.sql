USE AdventureWorks
SET STATISTICS TIME, IO ON
GO

BEGIN TRAN

INSERT INTO Person.Address (
	AddressLine1
	,AddressLine2
	,City
	,StateProvinceID
	,PostalCode
	,rowguid
	,ModifiedDate
	)
VALUES (
	N'1313 Mockingbird Lane'
	,N'Basement'
	,N'Springfield'
	,79
	,N'02133'
	,NEWID()
	,GETDATE()
	)

UPDATE a
SET a.[City] = 'Munro'
	,a.[ModifiedDate] = GETDATE()
FROM [Person].[Address] AS a
WHERE a.[City] = 'Monroe';

DELETE ea
FROM Person.EmailAddress AS ea
WHERE ea.BusinessEntityID = 42

ROLLBACK TRAN