USE AdventureWorks
SET STATISTICS IO, TIME ON
GO

DROP PROCEDURE IF EXISTS ComplexProcedure 
GO
DROP PROCEDURE IF EXISTS ComplexProcedureSales 
GO
DROP PROCEDURE IF EXISTS ComplexProcedureProducts 
GO
DROP PROCEDURE IF EXISTS ComplexProcedureProductsSales 
GO
DROP PROCEDURE IF EXISTS ComplexProcedureAll
GO

CREATE PROCEDURE ComplexProcedureSales @salesOrderId INT NULL
AS
BEGIN
	SELECT s.ProductID
		,s.LineTotal
		,s.OrderQty
	FROM sales.SalesOrderDetail AS s
	WHERE s.SalesOrderID = @salesOrderId	
END
GO
CREATE PROCEDURE ComplexProcedureProducts @productId INT NULL
AS
BEGIN
	SELECT s.ProductID
		,s.LineTotal
		,s.OrderQty
	FROM sales.SalesOrderDetail AS s
	WHERE s.productId = @productId
END
GO

CREATE PROCEDURE ComplexProcedureProductsSales @productId INT NULL
	,@salesOrderId INT NULL
AS
BEGIN
	SELECT s.ProductID
		,s.LineTotal
		,s.OrderQty
	FROM sales.SalesOrderDetail AS s
	WHERE s.productId = @productId
		AND s.SalesOrderID = @salesOrderId
END
GO

CREATE PROCEDURE ComplexProcedureAll
AS
BEGIN
	SELECT s.ProductID
		,s.LineTotal
		,s.OrderQty
	FROM sales.SalesOrderDetail AS s
END
GO

CREATE PROCEDURE ComplexProcedure @productId INT NULL
	,@salesOrderId INT NULL
AS
BEGIN
	IF @productId IS NOT NULL
		AND @salesOrderId IS NULL
	BEGIN
		EXEC ComplexProcedureProducts @productId
	END
	ELSE IF @productId IS NULL
		AND @salesOrderId IS NOT NULL
	BEGIN
		EXEC ComplexProcedureSales @salesOrderId
	END
	ELSE IF @productId IS NULL
		AND @salesOrderId IS NULL
	BEGIN
		EXEC ComplexProcedureProductsSales @productId
			,@salesOrderId
	END
	ELSE
	BEGIN
		EXEC ComplexProcedureAll
	END
END
GO



DBCC FREEPROCCACHE

EXEC ComplexProcedure 772, NULL
EXEC ComplexProcedure NULL, 43661

DBCC FREEPROCCACHE

EXEC ComplexProcedure NULL, 43661
EXEC ComplexProcedure 772, NULL

DROP PROCEDURE IF EXISTS ComplexProcedure 
GO