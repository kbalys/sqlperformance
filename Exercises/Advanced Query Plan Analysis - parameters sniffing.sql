USE AdventureWorks
SET STATISTICS IO, TIME ON
DBCC DROPCLEANBUFFERS
DBCC FREEPROCCACHE
GO

-- A very selective query - 1 result, plan uses nested loop
SELECT *
FROM [Person].[Address]
WHERE City = 'Mentor'

-- A non-selective query - 434 results, plan is index scan
SELECT *
FROM [Person].[Address]
WHERE City = 'London'

-- Add a variable City - plan parametrization as optimisation FOR UNKNOWN
DECLARE @City NVARCHAR(30)

SET @City = 'Mentor'

SELECT *
FROM [Person].[Address]
WHERE City = @City

-- equals to the following in stored procedures
-- OPTION (OPTIMIZE FOR UNKNOWN)
-- Fix - will use nested loop
SELECT *
FROM [Person].[Address]
WHERE City = @City
OPTION (RECOMPILE)

SET @City = 'London'

SELECT *
FROM [Person].[Address]
WHERE City = @City;
GO

-- Parameters sniffing
DROP PROCEDURE IF EXISTS dbo.AddressByCity 
GO

-- Create a procedure
CREATE PROCEDURE dbo.AddressByCity (@City NVARCHAR(30))
-- WITH RECOMPILE
AS
	SELECT *
	FROM [Person].[Address]
	WHERE City = @City
		-- uncomment the next line during the second run
		--OPTION (OPTIMIZE FOR UNKNOWN)
GO

-- Execute procedures - the order matters
DBCC DROPCLEANBUFFERS
DBCC FREEPROCCACHE

EXEC dbo.AddressByCity 'Mentor'
EXEC dbo.AddressByCity 'London'

-- Execute procedures - the order matters
DBCC FREEPROCCACHE
DBCC DROPCLEANBUFFERS

EXEC dbo.AddressByCity 'London'
EXEC dbo.AddressByCity 'Mentor'
-- Now uncomment "--OPTION (OPTIMIZE FOR UNKNOWN)" in procedure and compare results