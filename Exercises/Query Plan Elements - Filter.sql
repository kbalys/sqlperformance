USE AdventureWorks
SET STATISTICS TIME, IO ON
GO

SELECT a.[City]
	,COUNT(a.[City]) AS CityCount
FROM [Person].[Address] AS a
GROUP BY a.[City]
HAVING COUNT(a.[City]) > 1