USE AdventureWorks
SET STATISTICS IO, TIME ON
GO

-- Prior to 2012 calculating Running totals by using SELF-JOIN:
;WITH RunTotal
AS (
	SELECT ROW_NUMBER() OVER (
			PARTITION BY s.SalesPersonID ORDER BY s.OrderDate
			) AS [Row]
		,s.SalesPersonID
		,p.FirstName
		,p.LastName
		,s.TotalDue
		,s.OrderDate
	FROM Sales.SalesOrderHeader s
	JOIN Person.Person p ON s.SalesPersonID = p.BusinessEntityID
	)
SELECT a.SalesPersonID
	,a.FirstName
	,a.LastName
	,a.OrderDate
	,a.TotalDue
	,SUM(b.TotalDue) AS RunTotal
FROM RunTotal a
JOIN RunTotal b ON a.SalesPersonID = b.SalesPersonID
	AND a.[Row] >= b.[Row]
GROUP BY a.[Row]
	,a.SalesPersonID
	,a.FirstName
	,a.LastName
	,a.TotalDue
	,a.OrderDate
ORDER BY a.SalesPersonID
	,a.[Row]
-- Rewrite it using SUM() OVER