USE WideWorldImporters
SET STATISTICS IO, TIME ON
GO


DROP INDEX [FK_Sales_Invoices_CustomerID] ON [Sales].[Invoices]
GO

CREATE NONCLUSTERED INDEX [FK_Sales_Invoices_CustomerID] ON [Sales].[Invoices]
(
	[CustomerID] ASC,
	[OrderID] ASC
)

GO

SELECT i.OrderID
FROM Sales.Invoices AS i
WHERE i.CustomerID = 2

UNION

SELECT o.OrderID
FROM Sales.Orders AS o
WHERE o.PickedByPersonID = 2

-- ROLLBACK
DROP INDEX [FK_Sales_Invoices_CustomerID] ON [Sales].[Invoices]
GO

CREATE NONCLUSTERED INDEX [FK_Sales_Invoices_CustomerID] ON [Sales].[Invoices]
(
	[CustomerID] ASC
)