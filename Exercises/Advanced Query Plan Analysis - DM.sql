-- Available Performance Counters Groups
SELECT DISTINCT object_name AS PerformanceCounterGroup
FROM sys.dm_os_performance_counters
ORDER BY object_name

-- All Performance Counters
SELECT DISTINCT object_name AS PerformanceCounterGroup
	,counter_name
	,instance_name
	,cntr_value
FROM sys.dm_os_performance_counters
ORDER BY object_name
	,counter_name

-- Cache hit ration Performance Counter
SELECT (a.cntr_value * 1.0 / b.cntr_value) * 100.0 AS BufferCacheHitRatio
FROM sys.dm_os_performance_counters a
JOIN (
	SELECT cntr_value
		,OBJECT_NAME
	FROM sys.dm_os_performance_counters
	WHERE counter_name = 'Buffer cache hit ratio base'
		AND OBJECT_NAME LIKE '%Buffer Manager%'
	) b ON a.OBJECT_NAME = b.OBJECT_NAME
WHERE a.counter_name = 'Buffer cache hit ratio'
	AND a.OBJECT_NAME LIKE '%Buffer Manager%'



DECLARE @LazyWrites1 BIGINT;

SELECT @LazyWrites1 = cntr_value
FROM sys.dm_os_performance_counters
WHERE counter_name = 'Lazy writes/sec';

WAITFOR DELAY '00:00:10';

SELECT (cntr_value - @LazyWrites1) / 10 AS 'LazyWrites/sec'
FROM sys.dm_os_performance_counters
WHERE counter_name = 'Lazy writes/sec';