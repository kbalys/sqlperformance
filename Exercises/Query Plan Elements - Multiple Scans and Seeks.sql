USE AdventureWorks
SET STATISTICS TIME, IO ON
GO

-- Table Scan
SELECT dl.*
FROM dbo.DatabaseLog dl

-- Clustered Index Scan
SELECT ct.*
FROM Person.ContactType AS ct

-- Clustered Index Seek
SELECT ct.*
FROM Person.ContactType AS ct
WHERE ct.ContactTypeID = 7

-- Nonclustered Index Scan
SELECT ct.ContactTypeId
FROM Person.ContactType AS ct

-- Nonclustered Index Seek
SELECT ct.ContactTypeId
FROM Person.ContactType AS ct
WHERE ct.[Name] LIKE 'Own%'