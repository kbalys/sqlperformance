
USE AdventureWorksDW
GO


-- Cleanup
TRUNCATE TABLE FactInternetSalesTEST



INSERT INTO FactInternetSalesTEST 
SELECT TOP 3145725 * -- = 3 mln
FROM FactInternetSalesBig


SELECT * FROM sys.dm_db_column_store_row_group_physical_stats
WHERE object_id = OBJECT_ID('FactInternetSalesTEST')

SELECT * FROM sys.columns 
WHERE object_id = OBJECT_ID('FactInternetSalesTEST')



--	20101229 - 20140128
SELECT YEAR(OrderDate), count(*)
FROM FactInternetSalesTEST
GROUP BY YEAR(OrderDate)
ORDER BY YEAR(OrderDate)




-- min_data_id/ max_data_id
SELECT * FROM sys.column_store_segments  css
JOIN sys.partitions p ON p.partition_id = css.partition_id
WHERE object_id = OBJECT_ID('FactInternetSalesTEST')
	AND column_id = 2 -- OrderDateKey




-- + profiler
SET STATISTICS IO ON
SET STATISTICS TIME ON
GO

-- 17 000 pages
-- segment skipped 0
SELECT sum(salesamount), count(*), AVG(UnitPrice), AVG(ExtendedAmount), AVG(DiscountAmount), AVG(Freight) 
FROM FactInternetSalesTEST
WHERE OrderDateKey BETWEEN 20100101 AND 20101231






CREATE CLUSTERED INDEX CCI
ON FactInternetSalesTEST(OrderDateKey)
WITH DROP_EXISTING
GO

-- 18 s
CREATE CLUSTERED COLUMNSTORE INDEX CCI
ON FactInternetSalesTEST
WITH (DROP_EXISTING = ON, MAXDOP = 1)
GO





SELECT * FROM sys.dm_db_column_store_row_group_physical_stats
WHERE object_id = OBJECT_ID('FactInternetSalesTEST')

-- pages 450
-- segment skipped 2
SELECT sum(salesamount), count(*), AVG(UnitPrice), AVG(ExtendedAmount), AVG(DiscountAmount), AVG(Freight) 
FROM FactInternetSalesTEST
WHERE OrderDateKey BETWEEN 20100101 AND 20101231

