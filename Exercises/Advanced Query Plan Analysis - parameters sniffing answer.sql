USE AdventureWorks
SET STATISTICS IO, TIME ON
GO

DROP PROCEDURE IF EXISTS ComplexProcedure 
GO

CREATE PROCEDURE ComplexProcedure @productId INT NULL
	,@salesOrderId INT NULL
AS
BEGIN
	IF @productId IS NOT NULL
		AND @salesOrderId IS NULL
	BEGIN
		SELECT s.ProductID
			,s.LineTotal
			,s.OrderQty
		FROM sales.SalesOrderDetail AS s
		WHERE s.ProductID = @productId
	END
	ELSE IF @productId IS NULL
		AND @salesOrderId IS NOT NULL
	BEGIN
		SELECT s.ProductID
			,s.LineTotal
			,s.OrderQty
		FROM sales.SalesOrderDetail AS s
		WHERE s.SalesOrderID = @salesOrderId
	END
	ELSE IF @productId IS NULL
		AND @salesOrderId IS NULL
	BEGIN
		SELECT s.ProductID
			,s.LineTotal
			,s.OrderQty
		FROM sales.SalesOrderDetail AS s
	END
	ELSE
	BEGIN
		SELECT s.ProductID
			,s.LineTotal
			,s.OrderQty
		FROM sales.SalesOrderDetail AS s
		WHERE s.SalesOrderID = @salesOrderId
			AND s.ProductID = @productId
	END
END
GO

DBCC FREEPROCCACHE

EXEC ComplexProcedure 772, NULL
EXEC ComplexProcedure NULL, 43661

DBCC FREEPROCCACHE

EXEC ComplexProcedure NULL, 43661
EXEC ComplexProcedure 772, NULL

DROP PROCEDURE IF EXISTS ComplexProcedure 
GO