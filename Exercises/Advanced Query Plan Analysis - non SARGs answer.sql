USE AdventureWorks
SET STATISTICS TIME, IO ON
DBCC DROPCLEANBUFFERS
DBCC FREEPROCCACHE
GO

-- Remove non sarg 
SELECT w.ProductID
	,w.StartDate
FROM Production.WorkOrder w
WHERE w.StartDate >= '2013-01-01'
	AND w.StartDate < '2014-01-01'

-- Add missing index
CREATE NONCLUSTERED INDEX IX_Producttion_WorkOrder_StartDate ON [Production].[WorkOrder] ([StartDate]) INCLUDE ([ProductID])

-- Now compare number of reads

-- clean up
DROP INDEX [Production].[WorkOrder].IX_Producttion_WorkOrder_StartDate