USE AdventureWorks
SET STATISTICS TIME, IO ON
GO

-- click Include Live Query Statistics
SELECT *
FROM Person.Person AS p1
CROSS JOIN Person.Person AS p2

-- stop query