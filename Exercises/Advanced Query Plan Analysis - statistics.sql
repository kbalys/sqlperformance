USE AdventureWorks
SET STATISTICS IO, TIME ON
GO

DBCC SHOW_STATISTICS (
		'Sales.SalesOrderDetail'
		,'IX_SalesOrderDetail_ProductID'
		);

SELECT COUNT(ProductID) AS NumberOfRows
	,COUNT(DISTINCT ProductID) AS NumberOfDistinctRows
FROM Sales.SalesOrderDetail

SELECT ProductID
	,COUNT(ProductID) AS NumberOfProducts
FROM Sales.SalesOrderDetail
GROUP BY ProductID
ORDER BY ProductID