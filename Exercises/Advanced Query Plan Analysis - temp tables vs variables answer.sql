USE AdventureWorks
SET STATISTICS IO, TIME ON
GO

SELECT BusinessEntityID
	,LastName
INTO #tempTable
FROM Person.Person

-- Better plan - Hash Match
SELECT *
FROM #tempTable t
JOIN Person.Person p ON t.BusinessEntityID = p.BusinessEntityID

-- Compare with previous execution with variable.
DROP TABLE #tempTable