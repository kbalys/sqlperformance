-- create Orders table
USE Training
GO
DROP TABLE IF EXISTS Orders
DROP PARTITION SCHEME psOrders
DROP PARTITION FUNCTION pfOrders

USE Training
GO

SELECT OrderId, OrderDate
INTO Orders 
FROM WideWorldImporters.sales.Orders

SET STATISTICS IO ON

SELECT MONTH(OrderDate), COUNT(*)
FROM Orders
WHERE OrderDate >= '2015' AND OrderDate < '2016'
GROUP BY MONTH(OrderDate)
ORDER BY MONTH(OrderDate)
--Table 'Orders'. Scan count 1, logical reads 146, physical reads 0, read-ahead reads 0, lob logical reads 0, lob physical reads 0, lob read-ahead reads 0.

--CREATE PARTITION FUNCTION name (Column) AS RANGE RIGHT FOR VALUES(...);
--CREATE PARTITION SCHEME name AS PARTITION partitionFunction ALL TO ([Primary]);
--ALTER TABLE dbo.Orders ADD CONSTRAINT ptc PRIMARY KEY CLUSTERED (OrderId, OrderDate) ON partition_schema(column);









---



CREATE PARTITION FUNCTION pfOrders (DATE)
AS RANGE RIGHT FOR VALUES 
('2013-01-01', '2014-01-01', '2015-01-01', '2016-01-01');

CREATE PARTITION SCHEME psOrders AS PARTITION pfOrders ALL TO ([Primary]);

ALTER TABLE dbo.Orders ADD CONSTRAINT ptc PRIMARY KEY CLUSTERED (OrderId, OrderDate) ON psOrders(OrderDate);



INSERT Orders(OrderId, OrderDate) -- 2016
SELECT OrderId + 100000,DATEADD(year, 1, OrderDate) FROM Orders WHERE YEAR(OrderDate) = 2015

INSERT Orders(OrderId, OrderDate) -- 2017
SELECT OrderId + 200000,DATEADD(year, 2, OrderDate) FROM Orders WHERE YEAR(OrderDate) = 2015



SELECT MONTH(OrderDate), COUNT(*)
FROM Orders
WHERE OrderDate >= '2016' AND OrderDate < '2017'
GROUP BY MONTH(OrderDate)
ORDER BY MONTH(OrderDate)
--Table 'Orders'. Scan count 1, logical reads 22, physical reads 0, read-ahead reads 0, lob logical reads 0, lob physical reads 0, lob read-ahead reads 0.

















---






ALTER PARTITION SCHEME psOrders NEXT USED [Primary];
ALTER PARTITION FUNCTION pfOrders() SPLIT RANGE  ('2017-01-01');
