USE AdventureWorks
SET STATISTICS IO, TIME ON
GO

-- the same with ROW_NUMBER and CTE
;WITH MaxSalesOrderDetail
AS (
	SELECT ROW_NUMBER() OVER (
			PARTITION BY sod.SalesOrderId ORDER BY sod.LineTotal DESC, sod.SalesOrderDetailID
			) AS RowNum
		,sod.SalesOrderID
		,sod.SalesOrderDetailID
		,sod.LineTotal
	FROM Sales.SalesOrderDetail sod
	)
SELECT soh.SalesOrderID
	,soh.CustomerID
	,cte.SalesOrderDetailID
	,cte.LineTotal
FROM Sales.SalesOrderHeader soh
JOIN MaxSalesOrderDetail AS cte ON soh.SalesOrderID = cte.SalesOrderID
WHERE RowNum = 1
ORDER BY soh.SalesOrderID


-- version 2
;WITH MaxSalesOrderDetail2
AS (
	-- for each Sales Order return the most expensive line
	SELECT soh.SalesOrderID
		,soh.CustomerID
		,sod.SalesOrderDetailID
		,sod.LineTotal
		,ROW_NUMBER() OVER (
			PARTITION BY soh.SalesOrderID ORDER BY sod.LineTotal DESC, sod.SalesOrderDetailID
			) AS MostExpensive
	FROM Sales.SalesOrderHeader AS soh
	JOIN Sales.SalesOrderDetail sod ON sod.SalesOrderID = soh.SalesOrderID
	)
SELECT od.SalesOrderID
	,od.CustomerID
	,od.SalesOrderDetailID
	,od.LineTotal
FROM MaxSalesOrderDetail2 od
WHERE od.MostExpensive = 1
ORDER BY od.SalesOrderID;