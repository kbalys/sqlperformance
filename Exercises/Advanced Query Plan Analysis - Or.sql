USE WideWorldImporters
SET STATISTICS IO, TIME ON
GO

SELECT i.OrderID
FROM Sales.Invoices AS i
JOIN Sales.Orders AS o ON i.OrderID = o.OrderID
WHERE i.CustomerID = 2
	OR o.PickedByPersonID = 2
-- Task - try to improve above query