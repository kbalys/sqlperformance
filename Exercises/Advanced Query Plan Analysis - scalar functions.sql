USE AdventureWorks
SET STATISTICS IO, TIME ON
GO

DECLARE @ProductId INT = 1

-- First query shows cost = 0 which is 0% from batch
SELECT dbo.ufnGetStock(@ProductId)

-- The actual body of function shows what realy happens
DECLARE @ret INT;

SELECT @ret = SUM(p.[Quantity])
FROM [Production].[ProductInventory] p
WHERE p.[ProductID] = @ProductID
	AND p.[LocationID] = '6'

IF (@ret IS NULL)
	SET @ret = 0

SELECT @ret