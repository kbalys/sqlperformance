USE AdventureWorks
SET STATISTICS TIME, IO ON
GO

DROP TABLE IF EXISTS dbo.NextItem 
GO

CREATE TABLE dbo.NextItem (
		ID INT NOT NULL IDENTITY(1, 1) PRIMARY KEY CLUSTERED
		,IsUsed BIT NOT NULL
		)
GO

CREATE NONCLUSTERED INDEX IX_dbo_NextItem_IsUsed ON [dbo].[NextItem] ([IsUsed] ASC)

INSERT dbo.NextItem
VALUES (0)
	,(0)
	,(0)
	,(0)
	,(0)
	,(0)
	,(0)
	,(0)
	,(0)
	,(0)
GO

SELECT ni.*
FROM dbo.NextItem AS ni
ORDER BY ni.Id DESC

BEGIN TRAN

DECLARE @next INT

SELECT @next = ID
FROM dbo.NextItem AS ni
WHERE ni.IsUsed = 0
ORDER BY ni.Id DESC

SELECT @next

-- Execute up to this point
------------------------------------
-- Now execute Session 2 here 
------------------------------------
-- Try to execute the rest of the file

UPDATE ni
SET IsUsed = 1
FROM dbo.NextItem AS ni
WHERE ni.Id = 1 -- put the value

-- ROLLBACK TRAN
-- DROP TABLE dbo.NextItem